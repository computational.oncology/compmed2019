from pytrends.request import TrendReq
import numpy as np
import json
import csv 

pytrends = TrendReq(hl='en-US', tz=360)

codelist = ['DZ', 'AO', 'BJ', 'BW', 'IO', 'BF', 'BI', 'CV', 'CM', 'CF', 'TD', 'KM', 'CG', 'CD', 'CI', 'DJ', 'EG', 'GQ', 'ER', 'SZ', 'ET', 'TF', 'GA', 'GM', 'GH', 'GN', 'GW', 'KE', 'LS', 'LR', 'LY', 'MG', 'MW', 'ML', 'MR', 'MU', 'YT', 'MA', 'MZ', 'NA', 'NE', 'NG', 'RE', 'RW', 'SH', 'ST', 'SN', 'SC', 'SL', 'SO', 'ZA', 'SS', 'SD', 'TZ', 'TG', 'TN', 'UG', 'EH', 'ZM', 'ZW']

for code in codelist:
	# print(code)
	filename = code + ".csv"
	kw_list = ['flu']
	pytrends.build_payload(kw_list, cat=0, timeframe= '2018-01-01 2018-12-31', geo= code, gprop='')
	interest_over_time_df = pytrends.interest_over_time()
	interest_over_time_df.to_csv(filename, sep='\t')

print(interest_over_time_df)


# for item in interest_over_time_df:
# print(interest_over_time_df)

# with open('TrendsOverall.csv', mode='w') as TrendsOverall:
# 	TrendWriter = csv.writer(TrendsOverall, delimiter=',')
# 	for code in codelist:
# 		# print(code)
# 		kw_list = ['flu']
# 		pytrends.build_payload(kw_list, cat=0, timeframe= '2018-01-01 2018-12-31', geo= code, gprop='')
# 		interest_over_time_df = pytrends.interest_over_time()
# 		TrendWriter.writerow(interest_over_time_df)
	
