#import csv

#with open('FluNetInteractiveReport.csv', 'r') as csv_file:
#    csv_reader = csv.DictReader(csv_file)

#    for line in csv_reader:
#        print(line)


import csv

with open('FluNetInteractiveReport.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter='\t')

    with open('new3FluNetInteractiveReport.csv', 'w') as new_file:
        fieldnames = ["SDATE", "EDATE", "ALL_INF"]
        csv_writer = csv.DictWriter(new_file, fieldnames=fieldnames, delimiter=',')

        #csv_writer.writeheader()

        notneeded = ["Country", "WHOREGION", "FLUREGION", "Year", "SPEC_RECEIVED_NB", "SPEC_PROCESSED_NB", "AH1", "AH1N12009", "AH3", "AH5", "ANOTSUBTYPED", "INF_A", "BYAMAGATA", "BVICTORIA", "BNOTDETERMINED", "INF_B", "ALL_INF2", "TITLE"]
        for line in csv_reader:

            if "European Region of WHO" in str(line):
                for x in notneeded:
                    del line[x]
                csv_writer.writerow(line)
