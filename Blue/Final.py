import csv

#Takes the full CSV report file
#Gives out a CSV file with Europe cases only
def isolateEurope(input):
    with open(input, "r") as flu_csv:
        csv_reader = csv.reader(flu_csv, delimiter = "\t")

        with open("EuropeFlu.csv", "w") as Europe_csv:
            csv_writer = csv.writer(Europe_csv, delimiter = ",")

            for line in csv_reader:
                if "European Region of WHO" in line:
                    csv_writer.writerow(line)

#Takes the Europe CSV data
#Gives out CSV file with Date and Cases only!
def isolateDC(input):
    with open(input, "r") as Europe_csv:
        csv_reader = csv.reader(Europe_csv, delimiter = ",")

        with open("DateandCases.csv", "w") as DateCase_csv:
            csv_writer = csv.writer(DateCase_csv, delimiter = ",")

            for column in csv_reader:
                if(len(column) < 1):
                    continue
                csv_writer.writerow([column[4],column[18]])

#Takes CSV file of Dates and Cases for Europe
#Produces a dictionary with date as key and total cases as value
#i.e. dictionary of total cases for a particular date
def makeDict(input):
    DC_Dict = {}
    with open(input, "r") as DC_csv:
        csv_reader = csv.reader(DC_csv, delimiter = ",")

        for line in csv_reader:
            if(len(line) < 1):
                continue
            if line[0] in DC_Dict.keys():
                DC_Dict[line[0]] = DC_Dict[line[0]] + int(line[1])
            else:
                DC_Dict[line[0]] = int(line[1])
        return DC_Dict

#Produces a CSV file from the dictionary of dates and cases
def buildReport(input):
    with open(input, "w") as wEurope_csv:
        csv_writer = csv.writer(wEurope_csv, delimiter = ",")

        for date,case in DC_Dict.items():
            csv_writer.writerow((date,case))

isolateEurope("FluNetInteractiveReport.csv")
isolateDC("EuropeFlu.csv")
DC_Dict = makeDict("DateandCases.csv")
buildReport("weeklyEurope.csv")
