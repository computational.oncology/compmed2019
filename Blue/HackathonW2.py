from mwviews.api import PageviewsClient
import datetime
from pprint import pprint
import csv

p = PageviewsClient(user_agent="<person@organization.org> Flu analysis")

flu_views = p.article_views('en.wikipedia', ['Influenza'], granularity='daily', start='20180101', end='20181230')


def create_dict(input):
    """Creates dictionary of isocalendar dates against hits"""
    DV_Dict = {}
    for date, dict in input.items():
        correct_date = datetime.date.isocalendar(date)
        for article, hits in dict.items():
            DV_Dict[correct_date] = hits
    return(DV_Dict)


DV_dict = create_dict(flu_views)
# for date, dict in DV_Dict.items():
#     for date

def weeks(input):
    """Takes 1st day of week, adds it as new key, combines following hits"""
    output = {}
    for key, value in input.items():
        if key[2] == 1:
            output[key] = value
            i = key
        else:
            output[i] = output[i] + value
            pass
    return output

DV_dict1 = weeks(DV_dict)

def iso_year_start(iso_year):
    """The gregorian calendar date of the first day of the given ISO year"""
    fourth_jan = datetime.date(iso_year, 1, 4)
    delta = datetime.timedelta(fourth_jan.isoweekday()-1)
    return fourth_jan - delta

def iso_to_gregorian(iso_year, iso_week, iso_day):
    """Gregorian calendar date for the given ISO year, week and day"""
    year_start = iso_year_start(iso_year)
    return year_start + datetime.timedelta(days=iso_day-1, weeks=iso_week-1)

def converter(input):
    """Converts the isocalendar dates back to gregorian dates"""
    output = {}
    for key, item in input.items():
        output[iso_to_gregorian(key[0], key[1], key[2])] = item
    return output
#
DV_dict2 = converter(DV_dict1)

with open("wiki_weekly.csv", "w") as csv_new:
    """Writes a csv file to show the above"""
    fieldnames = ["Date","Hits"]
    csv_writer = csv.DictWriter(csv_new, fieldnames = fieldnames, delimiter = ",", extrasaction= "ignore")
    csv_writer.writeheader()
    for keys, values in DV_dict2.items():
        csv_writer.writerow({"Date" : keys, "Hits" : values})
