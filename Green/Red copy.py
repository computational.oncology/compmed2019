import csv
with open ("FluNetInteractiveReport.csv", "r", encoding="ISO-8859-1") as csv_file:
# Open the file in the same directory, r = read, csv_file is a variable
    myreader = csv.reader(csv_file)
    flu_list = list(myreader)

def selecting(the_list):
    wplist = []
    for line in the_list:
        newlist = line[0].split("\t")
        if newlist[1] == "Western Pacific Region of WHO":
            wplist.append(newlist)
    return wplist

wplist=selecting(flu_list)

with open("westernpacific.csv", "w") as newfile:
    mywriter = csv.writer(newfile, delimiter = "\n") # new separator, comma
    mywriter.writerow(wplist)

import datetime
from datetime import *

def simplify(the_list):
    simplelist = []
    for x in wplist:
        datestring = x[4]
        dt = datetime.strptime(datestring, "%d/%m/%Y")
        singlelist = [dt, x[4], x[5], x[0], x[18]]
        simplelist.append(singlelist)
    return simplelist

simplelist=simplify(wplist)
simplelist.sort()

with open("sortedwesternp.csv", "w") as newfile:
    mywriter = csv.writer(newfile, delimiter = "\n") #new separator, comma
    mywriter.writerow(simplelist)
#
# # Function definition
# # Input/pass in the list
# # First, collect the unique dates
# # For each unique date (i.e. group by each date), sum up the ALL_INF values for that date
# # Return a list with each unique date and its corresponding sum
def sum_by_date(the_list):
    # datelist to collect dates from the_list
    datelist=[]
    for x in the_list:
        datelist.append((x[1], x[2]))

    sumlist = [("Start Date","End Date","Number of Infections")] # sumlist to store each unique date and its corresponding sum
    for datetuple in datelist: # For each unique date
        mysum = 0 # Initialize to 0
        for x in the_list:
            if x[1] == datetuple[0]: # All dates that match unique date
                mysum = mysum + int(x[4]); # Use int() to convert character to integer
        singlelist = (datetuple, mysum)  # Sum for each unique date, i.e. group according to dates
        if singlelist not in sumlist:
            sumlist.append(singlelist)
    return sumlist

# Call function
# Input/pass in sortedlist from above
sumlist = sum_by_date(simplelist)

with open("weekly_infections.csv", "w") as newfile:
    mywriter = csv.writer(newfile, delimiter = "\n") #new separator, comma
    mywriter.writerow(sumlist)
