import csv

with open("flunetinteractivereport.csv", "r") as CSVFile:
	CSVReader = csv.DictReader(CSVFile, delimiter = "\t")

	with open("North America flu.csv", "w") as CSVNew:
		fieldnames = ["WHOREGION","SDATE","EDATE","SPEC_PROCESSED_NB"]

		CSVWriter = csv.DictWriter(CSVNew, fieldnames = fieldnames, delimiter = ",", extrasaction = "ignore")

		for line in CSVReader:
			for key,value in line.items():
				if value == "North America":
					CSVWriter.writerow(line)

