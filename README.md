# CompMed2019

This is the public Git repo for the CompMed 2019 course at Imperial College

Please DO NOT save in the top-level directory
Please make a directory with your initials or shortname (e.g. MW or MattW) and keep your work there
Let me know if any problems

