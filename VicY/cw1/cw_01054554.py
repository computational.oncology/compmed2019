#Takes patient data from a csv file, check the data
#Then categorise patients and output into new csv files

from datetime import datetime, timedelta, date
from collections import OrderedDict
import csv


youngest_dict = {"Non-anaemia":(110,200), "mild":(100,109), "moderate":(70,99), "severe":(0,70)}
young_dict = {"Non-anaemia":(115,200), "mild":(110,114), "moderate":(80,109), "severe":(0,80)}
shared_dict = {"Non-anaemia":(120,200), "mild":(110,119), "moderate":(80,109), "severe":(0,80)}
preg_dict = {"Non-anaemia":(110,200), "mild":(100,109), "moderate":(70,99), "severe":(0,70)}
men_dict = {"Non-anaemia":(130,200), "mild":(110,129), "moderate":(80,109), "severe":(0,80)}
hb_dict = {"6-59m":youngest_dict, "5-11y":young_dict, "12-14y":shared_dict, "non-pregnant":shared_dict, "preg":preg_dict, "men":men_dict}

non_anaemia_dict = OrderedDict()
mild_anaemia_dict = OrderedDict()
moderate_anaemia_dict = OrderedDict()
severe_anaemia_dict = OrderedDict()

#Compares the date of the data to the current date.
#If age of data is less than 1 year old, the data is added to a dictionary.
#The csv file is the input; the dictionary is the output
def filter_date(csv_file):
    with open(csv_file, "r") as pt_info:
        csv_reader = csv.reader(pt_info)

        next(csv_reader)

        output = OrderedDict()

        for line in csv_reader:
            data_date = datetime.strptime(line[1], "%d/%m/%Y %H:%M")
            last_year = (datetime.today().replace(microsecond=0)
                         - timedelta(days=365))
            if last_year < data_date:
                output[line[0]] = (line[1], line[2], line[3], line[4], line[5])
    return output


#Filter out male pregnancy (highly likely to be error in data)
def filter_male_pregnancy(input_dict):
    output = OrderedDict()

    for pt_id, pt_info in input_dict.items():
        if pt_info[2].upper() == "MALE" and pt_info[3].upper() == "Y":
            pass
        else:
            output[pt_id] = pt_info
    return output


#WHO defines polycythaemia as an Hb of >185g/l in men and >165g/l in women
#Will filter these patients out as it is improbable that they should be
#considered for anaemia
def filter_polycythaemmia(input_dict):
    output = OrderedDict()

    for pt_id, pt_info in input_dict.items():
        if int(pt_info[4]) > 185 and pt_info[2].upper() == "MALE":
            pass
        elif int(pt_info[4]) > 165 and pt_info[2].upper() == "female":
            pass
        else:
            output[pt_id] = pt_info
    return output


#Filter out pts with hb <6g/l (there is a case report of a pt who has
#survived and recovered from a hb of 6g/l), unlikely to be alive if lower
def filter_low_hb(input_dict):
    output = OrderedDict()

    for pt_id, pt_info in input_dict.items():
        if int(pt_info[4]) < 6:
            pass
        else:
            output[pt_id] = pt_info
    return output


#Calculate bday if patient's birthday is fed in as DD/MM/YYYY
def calculate_bday(input_bday):
    today = date.today()
    pt_bday = datetime.strptime(input_bday, "%d/%m/%Y")
    pt_year = today.year - pt_bday.year
    pt_month = today.month - pt_bday.month
    if (today.day - pt_bday.day) < 0:
        pt_month = pt_month - 1
    else:
        pass
    pt_age = pt_year*12 + pt_month
    return pt_age


# Filter out pts over the age of 123 (oldest person recorded is 122y164d)
def filter_old_age(input_dict):
    output = OrderedDict()

    for pt_id, pt_info in input_dict.items():
        pt_age = calculate_bday(pt_info[1])
        if int(pt_age) > 123*12:
            pass
        else:
            output[pt_id] = pt_info
    return output


# Filter out pregnant mothers older than 68 (oldest confirmed mother at 67y3d)
def filter_old_pregnancy(input_dict):
    output = OrderedDict()

    for pt_id, pt_info in input_dict.items():
        pt_age = calculate_bday(pt_info[1])
        if int(pt_age) > 68*12 and pt_info[3].upper() == "Y":
            pass
        else:
            output[pt_id] = pt_info
    return output


def find_anaemia(pt_id, pt_info, pt_population):
    for population, anaemia_table in hb_dict.items():
        if pt_population == population:
            for anaemic_status, hb_levels in anaemia_table.items():
                if int(pt_info[4]) >= hb_levels[0] and (int(pt_info[4])
                 <= hb_levels[1]):
                    return(anaemic_status)


def make_final_dict(pt_id, timestamp, haemoglobin, anaemic_status):
    if anaemic_status == "Non-anaemia":
        non_anaemia_dict[pt_id] = (timestamp, haemoglobin, anaemic_status)
    elif anaemic_status == "mild":
        mild_anaemia_dict[pt_id] = (timestamp, haemoglobin, anaemic_status)
    elif anaemic_status == "moderate":
        moderate_anaemia_dict[pt_id] = (timestamp, haemoglobin, anaemic_status)
    elif anaemic_status == "severe":
        severe_anaemia_dict[pt_id] = (timestamp, haemoglobin, anaemic_status)
    else:
        """set log here"""
        pass


def find_pt_status(input_dict):
    for pt_id, pt_info in input_dict.items():
        pt_age = calculate_bday(pt_info[1])
        if int(pt_age) >= 6 and int(pt_age) <= 59:
            pt_population = "6-59m"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)
        elif int(pt_age) >= 60 and int(pt_age) <= 143:
            pt_population = "5-11y"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)
        elif int(pt_age) >= 144 and int(pt_age) <= 179:
            pt_population = "12-14y"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)
        elif (pt_info[2].upper() == "FEMALE" and int(pt_age) >= 180
              and pt_info[3].upper() == "NO"
              or pt_info[3].upper() == "NA"):
            pt_population = "non-pregnant"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)
        elif (pt_info[2].upper() == "FEMALE" and int(pt_age) >= 180
              and pt_info[3].upper() == "Y"):
            pt_population = "preg"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)
        elif pt_info[2].upper() == "MALE" and int(pt_age) >= 180:
            pt_population = "men"
            anaemic_status = find_anaemia(pt_id, pt_info, pt_population)
            make_final_dict(pt_id, pt_info[0], pt_info[4], anaemic_status)


def combine_dicts(dictionary1, dictionary2):
    new_dictionary = OrderedDict()
    for pt_id1, pt_info1 in dictionary1.items():
        new_dictionary[pt_id1] = pt_info1
    for pt_id2, pt_info2 in dictionary2.items():
        new_dictionary[pt_id2] = pt_info2
    return new_dictionary


def write_csv(dictionary, csv_name):
    with open(csv_name, "w") as csv_new:
        fieldnames = ["PatientID","Timestamp","Haemoglobin", "Anaemia status"]
        csv_writer = csv.DictWriter(csv_new, fieldnames = fieldnames, delimiter = ",")
        csv_writer.writeheader()
        for pt_id, pt_info in dictionary.items():
            csv_writer.writerow({"PatientID" : pt_id, "Timestamp" : pt_info[0], "Haemoglobin": pt_info[1], "Anaemia status": pt_info[2]})




DATE_FILTERED = filter_date("example.csv")
print(DATE_FILTERED, "\n")

MALE_FILTERED = filter_male_pregnancy(DATE_FILTERED)
print(MALE_FILTERED, "\n")

POLY_FILTERED = filter_polycythaemmia(MALE_FILTERED)
print(POLY_FILTERED, "\n")

LOW_HB_FILTERED = filter_low_hb(POLY_FILTERED)
print(LOW_HB_FILTERED, "\n")

OLD_AGE_FILTERED = filter_old_age(LOW_HB_FILTERED)
print(OLD_AGE_FILTERED, "\n")

OLD_PREG_FILTERED = filter_old_pregnancy(OLD_AGE_FILTERED)
print(OLD_PREG_FILTERED, "\n")

find_pt_status(OLD_PREG_FILTERED)
print(non_anaemia_dict)
print(mild_anaemia_dict)
print(moderate_anaemia_dict)
print(severe_anaemia_dict)
combined_dict1 = combine_dicts(non_anaemia_dict, mild_anaemia_dict)
combined_dict2 = combine_dicts(moderate_anaemia_dict, severe_anaemia_dict)
write_csv(combined_dict1, "normal_mild.csv")
write_csv(combined_dict2, "moderate_severe.csv")






#TO DO LIST/WISHLIST
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#Filter out pregnant mothers younger than 5 (youngest confirmed mother at 5y7m)
#Filter out children under age of 6 months (algorithm unavailable for neonates)
#Filter out data from the future
#Filter out pts born in the future


# for key, value in MaleFiltered.items():
#     current_time = datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)
#     pt_bday = datetime.strptime(value[1], "%d/%m/%Y")
#     time_difference =  current_time - pt_bday
#     print(time_difference)
#     print(current_time)
