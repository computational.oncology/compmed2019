import csv

with open("flu.csv", "r") as CSVFile:
	CSVReader = csv.DictReader(CSVFile, delimiter = "\t")

	with open("NorthAmericaFlu.csv", "w") as CSVNew:
		fieldnames = ["WHOREGION","SDATE","SPEC_PROCESSED_NB"]

		CSVWriter = csv.DictWriter(CSVNew, fieldnames = fieldnames, delimiter = ",", extrasaction= "ignore")


		for line in CSVReader:
			for key,value in line.items():
				if value == "Region of the Americas of WHO":
					CSVWriter.writerow(line)